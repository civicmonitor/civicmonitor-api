<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Issue extends Model
{
        use Sluggable;
        use SoftDeletes;



        /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function positions()
    {
        return $this->hasMany(IssuePosition::class);
    }

    public function candidates()
    {
        return $this->hasManyThrough(Candidate::class, IssuePosition::class);
    }
    public function election() {
        return $this->belongsTo(Election::class);
    }
}
