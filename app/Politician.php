<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

use Illuminate\Database\Eloquent\SoftDeletes;

use Laravel\Scout\Searchable;

class Politician extends Model
{
    use softDeletes;
      use Searchable;
      use Sluggable;

      protected $casts = [
    'birth_date' => 'date',
    'death_date' => 'date',
    'start_date' => 'date',
    'end_date' => 'date',


];
    protected $fillable = ['name'];


  /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

public function Memberships() {
    return $this->hasMany(Membership::class);
}

  public function candidates() {
    return  $this->hasManyThrough(Candidate::class, Membership::class);
    // return  $this->hasMany(Candidate::class);
  } 


  public function political_parties()  {
    return  $this->hasManyThrough(PoliticalParty::class, Membership::class);
  }

  
 




    //
}
