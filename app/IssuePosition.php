<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssuePosition extends Model
{
    use SoftDeletes;
    use Sluggable;

    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public function issue()
    {
        return $this->belongsTo(Issue::class);
    }

    public function candidate () {
        return $this->belongsTo(Candidate::class);
    }

    

}
