<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PoliticalParty extends Model
{
        use Sluggable;

    protected $casts = [
    'founded_date' => 'date',
];

/**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'acronym'
            ]
        ];
    }   


}
