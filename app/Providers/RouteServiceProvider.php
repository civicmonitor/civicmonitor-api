<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use App\Politician;
use App\Candidate;
use App\Issue;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    // Route::model('politician', function ($value) {
    //         $politician = Politician::query();
    //         $politician = filter_var($value, FILTER_VALIDATE_INT) !== false ? $politician->where('id', $value) : $politician->where('slug', $value);
    //         $politician = $politician->first();
    //         return $politician;   
    // });

    Route::bind('politician', function($value, $route){
            $politician = Politician::query();
            $politician = filter_var($value, FILTER_VALIDATE_INT) !== false ? $politician->where('id', $value) : $politician->where('slug', $value);
            $politician = $politician->first();
            return $politician;   
});

    Route::bind('candidate', function($value, $route){
            $candidate = Candidate::query();
            $candidate = filter_var($value, FILTER_VALIDATE_INT) !== false ? $candidate->where('id', $value) : $candidate->where('slug', $value);
            $candidate = $candidate->first();
            return $candidate;   
});

  Route::bind('issue', function($value, $route){
            $issue = Issue::query();
            $issue = filter_var($value, FILTER_VALIDATE_INT) !== false ? $issue->where('id', $value) : $issue->where('slug', $value);
            $issue = $issue->first();
            return $issue;   
});
    

    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
