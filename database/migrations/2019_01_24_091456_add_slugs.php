<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('politicians', function($table) {
        $table->string('slug', 100)->nullable();
    });
        Schema::table('issues', function($table) {
        $table->string('slug', 100)->nullable();
    });
        Schema::table('issue_positions', function($table) {
        $table->string('slug', 100)->nullable();
    });
        Schema::table('political_parties', function($table) {
        $table->string('slug', 100)->nullable();
    });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
